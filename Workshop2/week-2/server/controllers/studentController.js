const Task = require("../models/studentModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const studentPost = (req, res) => {
  var task = new Task();

  task.firstname = req.body.firstname;
  task.lastname = req.body.lastname;
  task.email = req.body.email;
  task.address = req.body.address;

  if (task.firstname && task.lastname && task.email && task.address) {
    task.save(function (err) {
      if (err) {
        res.status(422);
        console.log('error while saving the task', err)
        res.json({
          error: 'There was an error saving the task'
        });
      }
      res.status(201); //CREATED
      res.header({
        'location': `http://localhost:3000/api/students/?id=${task.id}`
      });
      res.json(task);
    });
  } else {
    res.status(422);
    console.log('error while saving the task')
    res.json({
      error: 'No valid data provided for task'
    });
  }
};

/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const studentGet = (req, res) => {
  // if an specific task is required
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({
          error: "Task doesnt exist"
        })
      }
      res.json(task);
    });
  } else {
    // get all tasks
    Task.find(function (err, tasks) {
      if (err) {
        res.status(422);
        res.json({
          "error": err
        });
      }
      res.json(tasks);
    });

  }
};

/**
 * Updates a task
 *
 * @param {*} req
 * @param {*} res
 */
const studentPatch = (req, res) => {
  // get task by id
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({
          error: "Task doesnt exist"
        })
      }

      // update the task object (patch)
      task.firstname = req.body.firstname ? req.body.firstname : task.firstname;
      task.lastname = req.body.lastname ? req.body.lastname : task.lastname;
      task.email = req.body.email ? req.body.email : task.email;
      task.address = req.body.address ? req.body.address : task.address;
      // update the task object (put)
      // task.title = req.body.title
      // task.detail = req.body.detail

      task.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error while saving the student', err)
          res.json({
            error: 'There was an error saving the student'
          });
        }
        res.status(200); // OK
        res.json(task);
      });
    });
  } else {
    res.status(404);
    res.json({
      error: "Student doesnt exist"
    })
  }
};


const studentDelete = (req, res) => { //DELETE
  if (req.query && req.query.id) {
    Task.findByIdAndRemove(req.query.id, function (err, student) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the Student', err)
        res.json({
          error: "Student doesnt exist"
        })
      }
      res.status(204).send();
      res.json(student);
    });
  } else {
    if (err) {
      res.status(403);
      res.json({
        "Error.": err
      });
    }
    res.json(student);
  }
};

module.exports = {
  studentGet,
  studentPost,
  studentPatch,
  studentDelete
}