const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const client = new Schema({
  name: String,
  lastName: String,
  email: String,
  website: String,
});

module.exports = mongoose.model("Client", client);
