const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const order = new Schema({
  clientID: String,
  productsID: [String],
});

module.exports = mongoose.model("Order", order);
